import java.io.*;
import java.util.*;
public class house {

    public static void main(String args[])
    {
      String[] people = {"Lizzy", "Andrew", "Matt", "Michael", "Helen", "Megan"};
      String[] rooms = {"Ground Front", "Ground Back", "Middle Front", "Middle Back", "Top Front", "Top Back"};
      String placeHolder = "";
      int swapPos = 0;
      int secondPos = 0;
      boolean selection = false;
      String start = "";

      while (selection == false)
      {
          try {
            BufferedReader is = new BufferedReader(new InputStreamReader(System.in));
            start = is.readLine();
          } catch (IOException e) {
            System.out.println("IOException: " + e);
          }

          for (int i = 0; i < 200; i++)
            {
                swapPos = (int)(Math.random() * 6);

                placeHolder = people[swapPos];

                secondPos = (int)(Math.random() * 6);

                people[swapPos] = people[secondPos];
                people[secondPos] = placeHolder;
            }



          for (int i = 0; i < 6; i++)
          {
              System.out.format("%s has been assigned the %s room\n", people[i], rooms[i]);
          }

          System.out.println("Happy with the selections? Y for Yes, N for No\n");

          try {
            BufferedReader is = new BufferedReader(new InputStreamReader(System.in));
            placeHolder = is.readLine();
          } catch (IOException e) {
            System.out.println("IOException: " + e);
          }

          if (placeHolder.equals("Y"))
          {
            selection = true;
          }
          else
          {
            System.out.println("We're sorry you're not happy with the selections.");
          }
      }



    }
}
