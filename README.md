## Synopsis

A single java class (in current iteration). This piece of code will take the names of our 6 flatmates and randomly assign each one with one of the rooms in our new designated student house.

There is potential to change this into a server based java application, which will take the input of each flat mates and see if they are happy with the room they are assigned.

## Code

It's a piece of Java code. It's pretty simple and just mixes up the array of house mates in order to assign each one to a room. It does it 200 times but on request I could increase that, but 200 seemed reasonable enough on my first test of the program and it seems to fit the purpose.

Code takes input to check if the flatmates are satisfied with the positions at the end.

## Motivation

The project was designed as a solution to our house' problem of selecting which room each flatmate has. It essentially fulfills the same purpose that a hat with each room inside would fulfill, but this is handled entirely by the computer rather than leaving the responsibility to one flatmate to create each individual piece of paper containing a house name, which could be compromised in some way. Fairness is the primary purpose of this code.

It was designed to be cross-platform and fully functional. There should be little bugs contained within.

## Installation

Compile using javac. Run using java.

## Tests

Compile java and run java. Type "Start" to begin the code. Follow instructions on screen.

## Contributors

Anyone can contribute to this (Except from Andrew, who is exempt from contribution). It's simple java. Tweet me if you want.

## License

MIT License.

Andrew is not permitted to contribute towards the codebase.
